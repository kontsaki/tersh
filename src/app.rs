use std::{
    io::Read,
    path::PathBuf,
    process::{Command, Stdio},
    sync::{Arc, Mutex},
    fmt::Display,
    error::Error,
    os::unix::io::FromRawFd,
};

use nix::{pty, unistd::read};

use egui::Key;

pub struct Tersh {
    user: String,
    hostname: String,
    cwd: PathBuf,
    programs: Vec<Program>,
    input: String,
}

struct Program {
    name: String,
    command: String,
    process: process::Child,
    output: Arc<Mutex<String>>,
}

#[derive(Debug)]
enum ProcessError {
    IsInteractive,
}
impl Display for ProcessError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "process error")
    }
}
impl Error for ProcessError {}

struct Process {
    name: String,
}

impl Process {
    fn new(name: &str) -> Process {
        Process { name: name.to_owned() }
    }
      fn start() -> Result<(), String> {
        let pty::OpenptyResult { master, slave } = pty::openpty(None, None).unwrap();

        // SAFETY: no other functions should call `from_raw_fd`, so there
            // is only one owner for the file descriptor.
            let stdio = unsafe { Stdio::from_raw_fd(slave) };
        let vim = Command::new("vim")
        .stdin(Stdio::inherit())
        .stdout(stdio)
        .spawn()
        .expect("vim failed");


        let mut out = [0u8; 1024];
        read(master, &mut out).expect("Cannot read master");

        // heuristic to find out if command is interactive and requires an actual terminal
            if out.contains(&27u8) {
                eprintln!("Cannot run interactive terminal programs, atm");
            }
            Ok(())
    }}
impl Program {
    fn new(command: String) -> Self {
        // TODO: use shlex
        let mut parts = command.split_whitespace();
        let cmd = parts
            .next()
            .map(ToString::to_string)
            .expect("valid command");
        let mut child = process::Command::new(&cmd)
            .args(parts)
            .stdout(process::Stdio::piped())
            .spawn()
            .unwrap();

        let output = Arc::new(Mutex::new(String::new()));

        if let Some(mut stdout) = child.stdout.take() {
            let output = output.clone();
            std::thread::spawn(move || {
                let mut buffer = [0; 1024 * 64];
                loop {
                    if stdout.read(&mut buffer).unwrap() == 0 {
                        break;
                    }
                    let mut output = output.lock().expect("lock is poisoned");
                    *output += &String::from_utf8_lossy(&buffer).to_owned();

                }
            });
        };
        Self {
            name,
            command,
            process,
            output,
        }
    }
}

impl Default for Tersh {
    fn default() -> Self {
        Self {
            // Example stuff:
            user: std::env::var("USER").unwrap_or("$USER".to_string()),
            hostname: std::env::var("HOSTNAME").unwrap_or("$HOSTNAME".to_string()),
            cwd: std::env::current_dir().unwrap_or(PathBuf::default()),
            programs: Vec::new(),
            input: String::new(),
        }
    }
}

impl Tersh {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customized the look at feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        //        if let Some(storage) = cc.storage {
        //            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        //        }

        Default::default()
    }
}

impl eframe::App for Tersh {
    //    fn save(&mut self, storage: &mut dyn eframe::Storage) {
    //        eframe::set_value(storage, eframe::APP_KEY, self);
    //    }
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let Self {
            user,
            hostname,
            cwd,
            programs,
            input,
        } = self;

        egui::TopBottomPanel::top("status").show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                ui.heading(format!(
                    "{user} at {hostname} in {}",
                    cwd.to_str().unwrap_or("$PWD")
                ))
            })
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            // The central panel the region left after adding TopPanel's and SidePanel's

            ui.add(egui::TextEdit::singleline(input));
            if ui.input().key_pressed(Key::Enter) {
                tracing::info!("Running {}", command);
                Program::new(input.clone());
            }
        });

        if !output.lock().unwrap().is_empty() {
            egui::Window::new(program.as_ref().unwrap())
                .vscroll(true)
                .hscroll(true)
                .show(ctx, |ui| {
                    ui.label(output.lock().unwrap().to_owned());
                });
        }
        egui::TopBottomPanel::bottom("tersh_info").show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                ui.label("tersh v0.1");
                egui::warn_if_debug_build(ui);
            })
        });
        // Examples of how to create different panels and windows.
        // Pick whichever suits you.
        // Tip: a good default choice is to just keep the `CentralPanel`.
        // For inspiration and more examples, go to https://emilk.github.io/egui

        //        #[cfg(not(target_arch = "wasm32"))] // no File->Quit on web pages!
        //        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
        //            // The top panel is often a good place for a menu bar:
        //            egui::menu::bar(ui, |ui| {
        //                ui.menu_button("File", |ui| {
        //                    if ui.button("Quit").clicked() {
        //                        _frame.close();
        //                    }
        //                });
        //            });
        //        });

        //        egui::SidePanel::left("side_panel").show(ctx, |ui| {
        //            ui.heading("Side Panel");
        //
        //            ui.horizontal(|ui| {
        //                ui.label("Write something: ");
        //                ui.text_edit_singleline(label);
        //            });
        //
        //            ui.add(egui::Slider::new(value, 0.0..=10.0).text("value"));
        //            if ui.button("Increment").clicked() {
        //                *value += 1.0;
        //            }
        //
        //            ui.with_layout(egui::Layout::bottom_up(egui::Align::LEFT), |ui| {
        //                ui.horizontal(|ui| {
        //                    ui.spacing_mut().item_spacing.x = 0.0;
        //                    ui.label("powered by ");
        //                    ui.hyperlink_to("egui", "https://github.com/emilk/egui");
        //                    ui.label(" and ");
        //                    ui.hyperlink_to(
        //                        "eframe",
        //                        "https://github.com/emilk/egui/tree/master/crates/eframe",
        //                    );
        //                    ui.label(".");
        //                });
        //            });
        //        });

        //        if false {
        //            egui::Window::new("Window").show(ctx, |ui| {
        //                ui.label("Windows can be moved by dragging them.");
        //                ui.label("They are automatically sized based on contents.");
        //                ui.label("You can turn on resizing and scrolling if you like.");
        //                ui.label("You would normally chose either panels OR windows.");
        //            });
        //        }
    }
}
